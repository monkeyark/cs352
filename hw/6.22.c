#include <stdio.h>

#define MAX_PROCESSES 255

int number_of_processes = 0;

int main()
{
    // the implementation of fork() calls this function
    int allocate_process()
    {
        int new_pid;

        if (number_of_processes == MAX_PROCESSES)
        {
            return -1;
        }
        else
        {
            // allocate necessary process resources
            ++number_of_processes;
            return new_pid;
        }
    }

    // the implementation of exit() calls this function
    void release_process()
    {
        //release process resources
        --number_of_processes;
    }

    return 0;
}