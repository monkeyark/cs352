#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int a;

int main()
{
    int b;
    int c;
    a = 1;
    b = 2;
    c = 3;
    if (fork() == 0)
    {
        a = 10;
        b = 20;
        c = 30;
        if (fork() == 0)
        {
            c = 300;
            printf("a=%d     b=%d      c=%d\n",a,b,c);
            exit(0);
        }
        else
        {
            wait(NULL);
            printf("a=%d     b=%d      c=%d\n",a,b,c);
            a = 100;
            exit(0);
        }
    }
    else
    {
        b = 200;
        printf("a=%d     b=%d      c=%d\n",a,b,c);
        exit(0);
    }
    
    return 0;
}
