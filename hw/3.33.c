#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main()
{
    pid_t pid;
    pid = fork();

    if (pid < 0)
    {
        fprintf(stderr, "fork failed\n");
        return -1;
    }
    else if (pid == 0)
    {
        execlp("/bin/ls", "ls", NULL);
        printf("C: LINE J\n");
    }
    else
    {
        wait(NULL);
        printf("P: Child Complete\n");
    }

    return 0;
}
