#include <stdio.h>

typedef struct
{
    int available; // 0 indicates available, 1 indicates unavailable
} lock;

//initialization
lock *mutex = malloc(sizeof(lock));
mutex->available = 0;

compare_and_swap(int *value, int expected, int new_value)
{
    int temp = *value;
    if (*value = expected)
    {
        *value = new_value;
    }
    return temp;
}


void acquire(lock *mutex)
{
    while (compare_and_swap(&(mutex->available), 0, 1) != 0)
    ; // do nothing
}

void release(lock *mutex)
{
    mutex->available = 0;
}
