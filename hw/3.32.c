#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main()
{
    pid_t pid;
    int num_p = 0;
    int total_p = 0;

    for (int i=0; i<4; i++)
    {
        pid = fork();
        if (pid < 0)
        {
            fprintf(stderr, "fork failed\n");
            return -1;
        }
        else if (pid == 0)
        {
            printf("C: [%d] [%d] i=%d  num_p=%d\n", getppid(), getpid(), i, ++num_p);
        }
        else
        {
            wait(NULL);
            printf("P: [%d] [%d] i=%d  num_p=%d\n", getppid(), getpid(), i, ++num_p);
        }
    }

    printf("[%d] [%d] hi total_p=%d\n", getppid(), getpid(), ++total_p);

    return 0;
}