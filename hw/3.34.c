#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main()
{
    pid_t pid, pid1;
    pid = fork();

    if (pid < 0)
    {
        fprintf(stderr, "fork failed\n");
        return 1;
    }
    else if (pid == 0)
    {
        pid1 = getpid();
        printf("C: pid =  A %d\n", pid);
        printf("C: pid1 = B %d\n", pid1);
    }
    else
    {
        pid1 = getpid();
        printf("P: pid =  C %d\n", pid);
        printf("P: pid1 = D %d\n", pid1);
        wait(NULL);
    }

    return 0;
}
