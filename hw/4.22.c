#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
int N;
void *thread(void *x)
{
    int *id = (int *)x;
    while (N != *id);
    printf("Thread %d \n", *id);
    N--;
    pthread_exit(NULL);
}

int main()
{
    N = 0;
    int id1 = 1;
    int id2 = 2;
    int id3 = 3;
    pthread_t t1, t2, t3;
    printf("Parent creating threads\n");
    pthread_create(&t1, NULL, thread, &id1);
    pthread_create(&t2, NULL, thread, &id2);
    pthread_create(&t3, NULL, thread, &id3);
    printf("Threads created\n");
    N = 3;
    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    pthread_join(t3, NULL);
    printf("Threads are done\n");
    return 0;
}
