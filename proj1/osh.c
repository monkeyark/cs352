#include<stdio.h>
#include<unistd.h>
#include<string.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<fcntl.h>
#include<sys/stat.h>

#define MAXLINE 80 //The maximum length command
#define EXIT_SIG "exit"
#define HISTORY_SIG "!!"


//convert string to args as executable format for execvp
int split_str(char str[], char *args[])
{
    char *token;
    token = strtok(str, " ");
    int i = 0;
    while (token != NULL) //split fgets input to pointer of strings
    {
        args[i] = token;
        //printf("split_str                                                               %s\n", args[i]);
        args[i+1] = '\0'; //end of the command
        token = strtok(NULL, " ");
        i++;
    }
    
    return 0;
}

int divide_str(char str[], char *delimiter, char **left, char **right)
{
    *left = strtok(str, delimiter);
    *right = strtok(NULL, delimiter);
    return 0;
}

int delimiter_position(char c, char *str)
{
    int len = strlen(str);
    int position = -1;
    int i;
    for (i=0; i<len; i++)
    {
        if (str[i] == c)
        {
            position = i;
        }
    }
    return position;
}

void child_write(int fd, char *args[])
{
    dup2(fd, 1);
    close(fd);
    execvp(args[0], args);
}

void child_read(int fd, char *args[])
{
    dup2(fd, 0);
    close(fd);
    execvp(args[0], args);
}




int main(int argc, char *argv[])
{
    char *args[MAXLINE/2+1]; //command line arguments
    char *args_left[MAXLINE/2+1];
    char *args_right[MAXLINE/2+1];
    char input[MAXLINE];
    char history[MAXLINE];
    char *input_left = malloc(sizeof(char)*MAXLINE+1);
    char *input_right = malloc(sizeof(char)*MAXLINE+1);
    char cmd[MAXLINE]; //command tha will be executed
    memset(args, '\0', (MAXLINE/2+1)*sizeof(args[0])); //clear args memory
    memset(input, '\0', MAXLINE*sizeof(input[0])); //clear input array memory
    memset(history, '\0', MAXLINE*sizeof(history[0])); //clear input array memory
    memset(cmd, '\0', MAXLINE*sizeof(cmd[0]));
    int shouldrun = 1; //flag to determine when to exit program


    while (shouldrun)
    {
        printf("osh>");
        fflush(stdout);
        fgets(input, MAXLINE, stdin); //break with ^Z
        input[strlen(input)-1] = '\0'; //set the last char \n to null
        strcpy(cmd, input);
        split_str(cmd, args);


        if (args[0] == NULL) // if input is empty, go next iteration
        {
            continue;
        }


        if (strcmp(EXIT_SIG, args[0]) == 0) //exit programm if signal detected
        {
            shouldrun = 0;
            //continue;
            exit(0);
        }

        int history_run = 0; // history flag
        if (strcmp(HISTORY_SIG, args[0]) == 0) //check if history exist
        {
            if (strlen(history) == 0)
            {
                fprintf(stderr, "no commands in history\n");
                continue;
            }
            else
            {
                history_run = 1; //exectued command from history
            }
        }
        
        int pipefd[2];
        if (pipe(pipefd) < 0)
        {
            fprintf(stderr, "pipe error!\n");
            return 1;
        }




        if (history_run) // execute from history
        {
            if (delimiter_position('|', history) > 0) // if there is pipe
            {
                strcpy(cmd, history);
                divide_str(cmd, "|", &input_left, &input_right);
                split_str(input_left, args_left);
                split_str(input_right, args_right);

                int pid1 = fork();
                if (pid1 == 0)
                {
                    close(pipefd[1]);
                    dup2(pipefd[0], 0);
                    close(pipefd[0]);
                    execvp(args_right[0], args_right);
                    exit(0);
                }

                int pid2 = fork();
                if (pid2 == 0)
                {
                    close(pipefd[0]); // close read-end of pipe
                    dup2(pipefd[1], 1);
                    close(pipefd[1]);
                    execvp(args_left[0], args_left);
                    exit(0);
                }

                close(pipefd[0]);
                close(pipefd[1]);
                
                waitpid(pid2, NULL, 0);
                waitpid(pid1, NULL, 0);
            }
            else if (delimiter_position('>', history) > 0)
            {
                strcpy(cmd, history);
                divide_str(cmd, ">", &input_left, &input_right);
                split_str(input_left, args_left);
                split_str(input_right, args_right);

                int pid1 = fork();
                if (pid1 == 0) // child will read from pipe and write to a file
                {
                    close(pipefd[1]);
                    pipefd[0] = open(args_right[0], O_RDWR|O_TRUNC|O_CREAT, S_IRWXU|S_IROTH);
                    dup2(pipefd[0], STDOUT_FILENO);
                    close(pipefd[0]);
                    execvp(args_left[0], args_left);
                    exit(0);
                }
                else
                {
                    waitpid(pid1, NULL, 0);
                    close(pipefd[0]);
                    close(pipefd[1]);
                }
            }
            else if (delimiter_position('<', history) > 0)
            {
                strcpy(cmd, history);
                divide_str(cmd, "<", &input_left, &input_right);
                split_str(input_left, args_left);
                split_str(input_right, args_right);

                int pid1 = fork();
                if (pid1 == 0) // child will read from pipe and write to a file
                {
                    close(pipefd[0]);
                    pipefd[1] = open(args_right[0], O_RDONLY, S_IRWXU|S_IROTH);
                    dup2(pipefd[1], STDIN_FILENO);
                    close(pipefd[1]);
                    execvp(args_left[0], args_left);
                    exit(0);// MUST exit child here
                }
                else
                {
                    waitpid(pid1, NULL, 0);
                    close(pipefd[0]);
                    close(pipefd[1]);
                }
            }
            else
            {
                pid_t pid = fork(); //fork a child process
                if (pid == 0) //child process invoke execvp
                {
                    strcpy(cmd, history);
                    split_str(cmd, args);
                    execvp(args[0], args);
                    fprintf(stderr, "execvp fails\n");
                    exit(0);// MUST exit child here
                }
                else
                {
                    wait(NULL);
                }
            }
        }
        else // execute from current user input
        {
            if (delimiter_position('|', input) > 0)
            {
                strcpy(cmd, input);
                divide_str(cmd, "|", &input_left, &input_right);
                split_str(input_left, args_left);
                split_str(input_right, args_right);

                int pid1 = fork();
                if (pid1 == 0)
                {
                    close(pipefd[1]);
                    dup2(pipefd[0], 0);
                    close(pipefd[0]);
                    execvp(args_right[0], args_right);
                    exit(0);
                }

                int pid2 = fork();
                if (pid2 == 0)
                {
                    close(pipefd[0]); // close read-end of pipe
                    dup2(pipefd[1], 1);
                    close(pipefd[1]);
                    execvp(args_left[0], args_left);
                    exit(0);
                }

                close(pipefd[0]);
                close(pipefd[1]);
                
                waitpid(pid2, NULL, 0);
                waitpid(pid1, NULL, 0);

                strcpy(history, input);
            }
            else if (delimiter_position('>', input) > 0) // ls -l > out.txt
            {
                strcpy(cmd, input);
                divide_str(cmd, ">", &input_left, &input_right);
                split_str(input_left, args_left);
                split_str(input_right, args_right);

                int pid1 = fork();
                if (pid1 == 0) // child will read from pipe and write to a file
                {
                    close(pipefd[1]);
                    pipefd[0] = open(args_right[0], O_RDWR|O_TRUNC|O_CREAT, S_IRWXU|S_IROTH);
                    dup2(pipefd[0], STDOUT_FILENO);
                    close(pipefd[0]);
                    execvp(args_left[0], args_left);
                    exit(0);
                }
                else
                {
                    waitpid(pid1, NULL, 0);
                    close(pipefd[0]);
                    close(pipefd[1]);
                }
                strcpy(history, input);
            }
            else if (delimiter_position('<', input) > 0) // 
            {
                strcpy(cmd, input);
                divide_str(cmd, "<", &input_left, &input_right);
                split_str(input_left, args_left);
                split_str(input_right, args_right);

                int pid1 = fork();
                if (pid1 == 0) // child will read from pipe and write to a file
                {
                    close(pipefd[0]);
                    pipefd[1] = open(args_right[0], O_RDONLY, S_IRWXU|S_IROTH);
                    dup2(pipefd[1], STDIN_FILENO);
                    close(pipefd[1]);
                    printf("line312: going to execvp\n");
                    execvp(args_left[0], args_left);
                    fprintf(stderr, "execvp fails input:line 314\n");
                    exit(0);// MUST exit child here
                }
                else
                {
                    waitpid(pid1, NULL, 0);
                    close(pipefd[0]);
                    close(pipefd[1]);
                }
                strcpy(history, input);
            }
            else 
            {
                pid_t pid = fork(); //fork a child process
                if (pid == 0) //child process invoke execvp
                {
                    strcpy(cmd, input);
                    split_str(cmd, args);
                    execvp(args[0], args);
                    //fprintf(stderr, "execvp fails input\n");
                    exit(0);// MUST exit child here
                }
                else
                {
                    wait(NULL);
                }
                strcpy(history, input);
            }
        }
        
        
    }
    
    free(input_left);
    free(input_right);
    return 0;
}
