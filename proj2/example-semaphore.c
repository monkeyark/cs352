/*
 *   A simple semaphore demonstration.
 *
 *   Link with the pthread library:
 *      gcc thisfile.c -lpthread
 *
 */

#include <pthread.h>
#include <stdio.h>
#include <semaphore.h>


sem_t s1, s2;

void *thread1(void *arg)
{
  printf("Thread 1 signaling\n");
  sem_post(&s2);
  sem_wait(&s1);
  printf("Thread 1 past wait\n");
  return NULL;
}

void *thread2(void *arg)
{
  sem_wait(&s2);
  printf("Thread 2 past wait\n");
  printf("Thread 2 signaling\n");
  sem_post(&s1);
  return NULL;
}

int main()
{
  sem_init(&s1, 0, 0);
  sem_init(&s2, 0, 0);

  pthread_t t1, t2;
  
  pthread_create(&t2, NULL, thread2, NULL);
  pthread_create(&t1, NULL, thread1, NULL);

  pthread_join(t1, NULL);
  pthread_join(t2, NULL);

  sem_destroy(&s1);
  sem_destroy(&s2);

  printf("Threads are done\n");
  return 0;
}

