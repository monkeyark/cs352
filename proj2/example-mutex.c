#include <unistd.h>
#include <stdio.h>
#include <pthread.h>

char buffer; 
int buffer_has_item = 0; 
pthread_mutex_t mutex; 

void writer_function(void) 
{ 
	int n = 0;
	while(n < 3) { 
	      pthread_mutex_lock( &mutex ); 
	      if ( buffer_has_item == 0 ) {
		    printf("Writer writes to buffer\n");
			buffer = 'c'; 
		    buffer_has_item = 1; 	
		  } 
	      pthread_mutex_unlock( &mutex ); 
	      n++;
		  sleep(2);
	} 
} 

void *reader_function(void *arg) 
{ 
	int n = 0;
	while(n < 3) { 
	      pthread_mutex_lock( &mutex );
	      if ( buffer_has_item == 1) { 
		    printf("Reader reads from buffer\n");
		    char consume_item = buffer; 		    
			buffer_has_item = 0; 
	      } 
	      pthread_mutex_unlock( &mutex ); 
	      n++;
		  sleep(2);
	}
 } 

int main() 
{ 
	pthread_t reader; 
	pthread_mutex_init(&mutex, NULL); 
	pthread_create(&reader, NULL, reader_function, NULL); 
	writer_function(); 
	pthread_join(reader, NULL);
    pthread_mutex_destroy(&mutex);
	return 0;
} 


