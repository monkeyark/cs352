/*
 * A simple illustration of pthread condition variables
 *
 * Link with the pthread library:  gcc thisfile.c -lpthread
 *
 */

#include <stdio.h>
#include <pthread.h>

int x =0;
pthread_mutex_t mutex;
pthread_cond_t cond;

void* thread1(void *arg)
{
  // wait until x>5
  pthread_mutex_lock(&mutex);
  while (x<=5)
    pthread_cond_wait(&cond, &mutex);
  pthread_mutex_unlock(&mutex);
  printf("Thread 1 awake...x>5\n");
  return NULL;
}

void* thread2(void *arg)
{
  int i;
  for (i=1; i<10; i++) {
    printf("Thread 2 setting x to %d\n", i);
    pthread_mutex_lock(&mutex);
    x = i;
    if (x>5) pthread_cond_signal(&cond);
    pthread_mutex_unlock(&mutex);
  }
  return NULL;
}

int main()
{
  pthread_mutex_init(&mutex, NULL);
  pthread_cond_init(&cond, NULL);
  
  pthread_t t1, t2;
  pthread_create(&t1, NULL, thread1, NULL);
  pthread_create(&t2, NULL, thread2, NULL);

  pthread_join(t1, NULL);
  pthread_join(t2, NULL);

  pthread_mutex_destroy(&mutex);
  pthread_cond_destroy(&cond);
  return 0;
}
