#include <stdio.h>
#include<string.h>
#include<stdlib.h>

#define MAXLINE 80 //The maximum length command
#define EXIT_SIG "exit"

//convert string to args as executable format for execvp
int split_str(char str[], char *args[])
{
    char *token;
    token = strtok(str, " ");
    int i = 0;
    while (token != NULL) //split fgets input to pointer of strings
    {
        args[i] = token;
        //printf("split_str                                                               %s\n", args[i]);
        args[i+1] = '\0'; //end of the command
        token = strtok(NULL, " ");
        i++;
    }
    
    return 0;
}

int main(int argc, char *argv[])
{
    char *args[MAXLINE]; //command line arguments
    char file_path[MAXLINE];
    char buffer_size[MAXLINE];
    char temp[MAXLINE]; //command tha will be executed
    //char *path_input[MAXLINE / 2 + 1];
    //char *path_output[MAXLINE / 2 + 1];

    memset(args, '\0', (MAXLINE / 2 + 1) * sizeof(args[0])); //clear args memory
    memset(file_path, '\0', MAXLINE * sizeof(file_path[0])); //clear input array memory
    memset(temp, '\0', MAXLINE*sizeof(temp[0]));
    int shouldrun = 1; //flag to determine when to exit program
    int buffer;

    while (shouldrun)
    {
        printf(">encrypt ");
        fflush(stdout);
        fgets(file_path, MAXLINE, stdin); //break with ^Z
        file_path[strlen(file_path) - 1] = '\0'; //set the last char \n to null
        strcpy(temp, file_path);
        split_str(temp, args);

        if (args[0] == NULL) continue; // if input is empty, go next iteration
        if (strcmp(EXIT_SIG, args[0]) == 0) //exit programm if signal detected
        {
            shouldrun = 0;
            exit(0);
        }

        printf("Enter buffter size: ");
        fflush(stdout);
        fgets(buffer_size, MAXLINE, stdin); //break with ^Z
        buffer_size[strlen(file_path) - 1] = '\0'; //set the last char \n to null

        if (buffer_size == NULL) continue; // if input is empty, go next iteration
        if (strcmp(EXIT_SIG, buffer_size) == 0) //exit programm if signal detected
        {
            shouldrun = 0;
            exit(0);
        }
        buffer = atoi(buffer_size);

        printf("input: %s    output: %s\n", args[0],args[1]);
        printf("buffer size is: %d\n", buffer);
    }

    return 0;
}